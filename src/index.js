const { ApolloServer, gql, PubSub } = require("apollo-server");

const typeDefs = gql`
  type Query {
    hello(name: String): String
    user: User
  }

  type User {
    id: ID!
    username: String
    firstLetterOfUsername: String
  }

  type Error {
    field: String!
    message: String!
  }

  type RegisterResponse {
    errors: [Error]
    user: User
  }

  input userInfo {
    username: String!
    password: String!
    age: Int
  }

  type Mutation {
    register(userInfo: userInfo!): RegisterResponse!
    login(userInfo: userInfo!): String!
  }

  type Subscription {
    newUser: User!
  }
`;

const NEW_USER = "NEW_USER";

const resolvers = {
  Subscription: {
    newUser: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator(NEW_USER),
    },
  },
  User: {
    firstLetterOfUsername: parent => {
      return parent.username ? parent.username[0] : null;
    },
    // username: parent => {
    //   console.log(parent);
    //   return parent.username;
    // },
  },
  Query: {
    hello: (parent, { name }) => {
      return `hey ${name}`;
    },
    user: () => ({
      id: 1,
      username: null,
    }),
  },
  Mutation: {
    login: (parent, { userInfo: { username } }, context, info) => {
      // check the password
      // await checkpassword(password);
      console.log(context);
      return username;
    },

    register: (_, { userInfo: { username } }, { pubsub }) => {
      //errors: null,
      const user = {
        id: 1,
        username,
      };
      pubsub.publish(NEW_USER, {
        newUser: user,
      });
      return {
        errors: [
          {
            field: "username",
            message: "bad",
          },
          {
            field: "username2",
            message: "bad2",
          },
        ],

        user,
      };
    },
  },
};

const pubsub = new PubSub();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: (req, res) => ({ req, res, pubsub }),
});

server.listen().then(({ url }) => console.log(`server started at ${url}`));
